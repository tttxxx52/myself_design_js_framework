class member_profile_do_insert_action extends ActionHandler{
    constructor(module, action, position_id){
        super(module, action);
        this.position_id = position_id;
    }
    prepareArgs(){
        this.addArgsById('name');
        this.addArgsById('email');
        this.addArgsById('tel');
        this.addArgsById('addr');
    }
    showResult(xhttp){
        if (xhttp.readyState === 4 && xhttp.status === 200) {
            document.getElementById(this.position_id).innerHTML = xhttp.responseText;
        }
        else{
            document.getElementById(this.position_id).innerHTML = xhttp.statusText;
        }
    }
}