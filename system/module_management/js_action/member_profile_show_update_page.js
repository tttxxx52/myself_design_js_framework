class member_profile_show_update_page extends ActionHandler{
    constructor(module, action, position_id){
        super(module, action);
        this.position_id = position_id;
    }
    prepareArgs(){
        var id = document.getElementsByName('id');
        var value='';
        for(var i=0; i<id.length; i++){
            if(id[i].checked){
                value = id[i].value;
                break;
            }
        }
        this.php_action = 'do_select_action';
        this.addArgs('where_statement', 'id = ' + value);
    }
    showResult(xhttp){
        var ResponseText;
        if (xhttp.readyState === 4 && xhttp.status === 200) {
            var json_str = xhttp.responseText;
            var obj = JSON.parse(json_str);
            if(obj['error'] === 0){
                var ds = obj['data_set'];
                var str = "<input type='hidden' name= 'id' id='id' value='" + ds[0]['id'] +"'><br/>";
                str += "姓名：<input type='text' name='name' id='name' value='" + ds[0]['name'] + "'><br/>";
                str += "email：<input type='text' name='email' id='email' value='" + ds[0]['email'] + "'><br/>";
                str += "電話：<input type='text' name='tel' id='tel' value='" + ds[0]['tel'] + "'><br/>";
                str += "住址：<input type='text' name='addr' id='addr' value='" + ds[0]['addr'] + "'><br/>";
                str += `<input type='button' value='修改' onclick="(new member_profile_do_update_action('member_profile','do_update_action', '%s')).run()">`;
                str = str.replace("%s", this.position_id);
                document.getElementById(this.position_id).innerHTML = str;
            
                this.loadModuleScript(this.module, "do_update_action");
            }
            else{
                document.getElementById(this.position_id).innerHTML = obj['error_message'];
            }
       }
        else{
            document.getElementById(this.position_id).innerHTML = xhttp.statusText;
        }
    }
}            