class member_profile_show_insert_page extends ActionHandler{
    constructor(module, action, position_id){
        super(module, action);
        this.position_id = position_id;
    }
    prepareArgs(){
        this.php = false;
    }
    showResult(xhttp){
        var str=`
        姓名：<input type='text' name='name' id='name' value=''><br/>
        email：<input type='text' name='email' id='email' value=''><br/>
        電話：<input type='text' name='tel' id='tel' value=''><br/>
        住址：<input type='text' name='addr' id='addr' value=''><br/>
        <input type='button' value='新增' onclick="(new member_profile_do_insert_action('member_profile','do_insert_action', '%s')).run()">
        `;
        str = str.replace("%s", this.position_id);
        document.getElementById(this.position_id).innerHTML = str;
        
        this.loadModuleScript(this.module, 'do_insert_action');
    }
}
            