class user_profile_do_select_action extends ActionHandler{
    constructor(module, action, position_id){
        super(module, action);
        this.position_id = position_id;
    }
    prepareArgs(){
        this.php = true;
        this.addArgs('where_statement', '');
    }
    ajax_success(json_str){
        try{
            var obj = JSON.parse(json_str);
            if(obj['status_code'] === 0){
                var content='編號 姓名 email 電話 住址 <br>';
                var ds = obj['data_set'];
                for(var index in ds){
                    for(var key in ds[index]){
                        content += ds[index][key] + ' ';
                    }
                    content += '<br>';
                }
                $('#'+this.position_id).html(content);
            }
            else{
                $('#'+this.position_id).html(obj['status_message']);
            }
        } catch(e){
            var msg = e + "<br>";
            msg += "JSON String: " + json_str;
            $('#'+this.position_id).html(msg);
        }
    }
    ajax_error(msg){
        $('#'+this.position_id).html(msg.status);
    }
}