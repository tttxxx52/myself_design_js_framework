class user_profile_do_delete_action extends ActionHandler{
    constructor(module, action, position_id){
        super(module, action);
        this.position_id = position_id;
    }
    prepareArgs(){
        var value = $('input[name=id]:checked').val();
        this.addArgs('id', value);
    }
    ajax_success(json_str){
        $('#'+this.position_id).html(json_str);
    }
    ajax_error(msg){
        console.log(msg.status);
    }
}