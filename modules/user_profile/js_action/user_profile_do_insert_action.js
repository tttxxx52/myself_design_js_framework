class user_profile_do_insert_action extends ActionHandler{
    constructor(module, action, position_id){
        super(module, action);
        this.position_id = position_id;
    }
    prepareArgs(){
        this.addArgsById('name');
        this.addArgsById('email');
        var value = $('#password').val();
        value = CryptoJS.MD5(value);
        this.addArgs('password', value);
        this.addArgsById('tel');
        this.addArgsById('addr');
    }
    ajax_success(json_str){
        $('#'+this.position_id).html(json_str);
    }
    ajax_error(msg){
        console.log(msg.status);
    }
}