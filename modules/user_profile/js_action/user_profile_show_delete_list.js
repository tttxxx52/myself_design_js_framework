class user_profile_show_delete_list extends ActionHandler{
    constructor(module, js_action, position_id){
        super(module, js_action);
        this.position_id = position_id;
    }
    prepareArgs(){
        this.php = true;
        this.php_action = 'do_select_action';
        this.addArgs('where_statement', '');
    }
    ajax_success(json_str){
        try{
            var obj = JSON.parse(json_str);
            if(obj['status_code'] === 0){
                var ds = obj['data_set'];
                var content='';
                for(var index in ds){
                    content += "<input type=radio name='id' id='id' value=" + ds[index]['id'] +">" + " ";
                    for(var key in ds[index]){
                        if(key !== 'id'){
                            content += ds[index][key] + ' ';
                        }
                    }
                    content += '<br>';
                }
                var str=`
                    <input type='button' value='刪除' onclick="(new user_profile_do_delete_action('user_profile','do_delete_action','%s')).run()">
                `;
                str = str.replace("%s", this.position_id);
                content += str;
                $('#'+this.position_id).html(content);
                this.loadModuleScript(this.module, 'do_delete_action');
            }
            else{
                $('#'+this.position_id).html(obj['status_message']);
            }
        } catch(e){
            var msg = e + "<br>";
            msg += "JSON String: " + json_str;
            $('#'+this.position_id).html(msg);
        }
    }
    ajax_error(msg){
        console.log(msg.status);
    }
}            