class login_do_login_action extends ActionHandler{
    constructor(module, action, position_id){
        super(module, action);
        this.position_id = position_id;
    }
    prepareArgs(){
        this.php = true;
        this.addArgsById('email');
        var value = $('#password').val();
        value = CryptoJS.MD5(value);
        this.addArgs('password', value);
    }
    ajax_success(json_str){
        try{ 
            var obj = JSON.parse(json_str);
                if(obj['status_code'] === 0){
                    (new home_show_home_page("home", "show_home_page", "body")).run();
                }
                else{
                    alert(obj['status_message']);
                }
            } catch(e){
                var msg = e + "<br>";
                msg += "JSON String: " + json_str;
                $('#'+this.position_id).html(msg);
            }
    }
    ajax_error(msg){
        console.log(msg.status);
    }
}