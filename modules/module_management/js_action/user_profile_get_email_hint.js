class user_profile_get_email_hint extends ActionHandler{
    constructor(module, action, position_id){
        super(module, action);
        this.position_id = position_id;
    }
    prepareArgs(){
        this.php = true;
        var email_value = document.getElementById('email').value;
        var where_statement = "email like '%" + email_value + "%'";
        this.addArgs('where_statement', where_statement);
        this.php_action = 'do_select_action';
    }
    showResult(xhttp){
        var json_str;
        if (xhttp.readyState === 4 && xhttp.status === 200) {
            json_str = xhttp.responseText;
            var obj = JSON.parse(json_str);
            if(obj['error'] === 0){
                var ds = obj['data_set'];
                var content='';
                for(var index in ds){
                    content += "<input type=radio name='id' id='id' value=" + ds[index]['id'] +">" + " ";
                    for(var key in ds[index]){
                        if(key != 'id') content += ds[index][key] + ' ';
                    }
                    content += '<br>';
                }
                document.getElementById(this.position_id).innerHTML = content;
            }
            else{
                document.getElementById(this.position_id).innerHTML = obj['error_message'];
            }
        }
        else{
            document.getElementById(this.position_id).innerHTML = xhttp.statusText;
        }
    }
}