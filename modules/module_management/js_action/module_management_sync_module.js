class module_management_sync_module extends ActionHandler{
    constructor(module, action, position_id){
        super(module, action);
        this.position_id = position_id;
    }
    prepareArgs(args){
        this.args = args;
        this.php_action = 'do_update_action';
        var args = this.args;
        this.addArgs('id', args[0]);
        this.addArgs('version', args[1]);
    }
    ajax_success(json_str){
        try{
            var obj = JSON.parse(json_str);
            if(obj['status_code'] === 0){
                var mm_smp = new module_management_show_management_page("module_management", "show_management_page", this.position_id);
                mm_smp.run();
            }
            else{
                this.show_dialog('模組更新', obj['status_message']);
            }
        } catch(e){
            var msg = e + "<br>";
            msg += "JSON String: " + json_str;
            this.show_dialog('模組更新', msg);
        }
    }
    ajax_error(msg){
        this.show_dialog('模組更新', msg.status);
    }
}
            