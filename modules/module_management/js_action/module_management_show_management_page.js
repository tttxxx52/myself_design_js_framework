class module_management_show_management_page extends ActionHandler{
    constructor(module, action, position_id){
        super(module, action);
        this.position_id = position_id;
    }
    prepareArgs(){
        this.php = true;
        this.php_action = 'show_module_list';
        this.addArgs('where_statement', '');
    }
    ajax_success(json_str){
        try{
            var obj = JSON.parse(json_str);
            console.log(obj);
            if(obj['status_code'] === 0){
                var ds = obj['data_set'];
                var content='<table>';
                content += "<tr><td>模組</td><td>型態</td><td>版本</td><td></td><td><button id='newModule'>安裝新模組</button></td><td></td></tr>";
                for(var index in ds){
                    content += '<tr>';
                    content += '<td>' + ds[index]['module_name'] + '</td>';
                    content += '<td>' + ds[index]['module_type'] + '</td>';
                    content += '<td>' + ds[index]['module_version'] + '</td>';
                    if(ds[index]['module_version'] != ds[index]['physical_version']){
                        content += "<td><button id='syncModule' value='" + index + "'>同步</button></td>";
                    }
                    else{
                        content += '<td></td>';
                    }
                    content += "<td><button id='upgradeModule' value='" + ds[index]['module_id'] + "'>升級</button><button id='removeModule' value='" + ds[index]['module_id'] + "'>移除</button></td>";
                    if(ds[index]['module_version'] != ds[index]['physical_version']){
                        content += "<td>實體版本：" + ds[index]['physical_version'] + "</td>";
                    }
                    else{
                        content += '<td></td>';
                    }
                    content += '</tr>';
                }
                content += '</table>';
                $('#'+this.position_id).html(content);
                var self = this;
                $("#newModule").on('click', function(){
                    var mm_nm = new module_management_new_module("module_management", "new_module", self.position_id);
                    mm_nm.run();
                });
                $('[id="syncModule"]').on('click', function(){
                    var mm_sm = new module_management_sync_module("module_management", "sync_module", self.position_id);
                    var idx = $(this).val();
                    mm_sm.run(new Array(ds[idx]['module_id'], ds[idx]['physical_version']));
                });
                $('[id="upgradeModule"]').on('click', function(){
                    self.show_dialog('升級模組', '升級模組尚未完成，開發中'+$(this).val());
                });
                $('[id="removeModule"]').on('click', function(){
                    self.show_dialog('移除模組', '移除模組尚未完成，開發中'+$(this).val());
                });
                this.loadModuleScript(this.module, "sync_module");
                this.loadModuleScript(this.module, "new_module");
            }
            else{
                $('#'+this.position_id).html(obj['status_message']);
            }
        } catch(e){
            var msg = e + "<br>";
            msg += "JSON String: " + json_str;
            $('#'+this.position_id).html(msg);
        }
    }
    ajax_error(msg){
        $('#'+this.position_id).html(msg.status);
    }
    
//    showResult(xhttp){
//        var str=`
//        尚未修改，請勿使用<br>
//        <input type='button' value='新增模組' onclick="(new module_management_show_insert_page('module_management','show_insert_page','show_area')).run()">
//        <input type='button' value='更新模組' onclick="(new module_management_show_update_list('module_management','show_update_list','show_area')).run()">
//        <input type='button' value='覆蓋模組' onclick="(new module_management_show_delete_list('module_management','show_delete_list','show_area')).run()">
//        <input type='button' value='查詢模組' onclick="(new module_management_do_select_action('module_management','do_select_action','show_area')).run()">
//        <input type='button' value='模組權限' onclick="(new module_management_show_privilege_page('module_management','show_privilege_page','show_area')).run()">
//        <br>
//        <div id="show_area"></div>
//        `;
//        document.getElementById(this.position_id).innerHTML = str;
//        
//        this.loadModuleScript(this.module, "show_insert_page");
//        this.loadModuleScript(this.module, "show_update_list");
//        this.loadModuleScript(this.module, "show_delete_list");
//        this.loadModuleScript(this.module, "do_select_action");
//        this.loadModuleScript(this.module, "show_privilege_page");
//    }
}
            