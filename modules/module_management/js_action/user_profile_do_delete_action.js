class user_profile_do_delete_action extends ActionHandler{
    constructor(module, action, position_id){
        super(module, action);
        this.position_id = position_id;
    }
    prepareArgs(){
        var id = document.getElementsByName('id');
        var value='';
        for(var i=0; i<id.length; i++){
            if(id[i].checked){
                value = id[i].value;
                break;
            }
        }
        this.addArgs('id', value);
    }
    showResult(xhttp){
        if (xhttp.readyState === 4 && xhttp.status === 200) {
            document.getElementById(this.position_id).innerHTML = xhttp.responseText;
        }
        else{
            document.getElementById(this.position_id).innerHTML = xhttp.statusText;
        }
    }
}