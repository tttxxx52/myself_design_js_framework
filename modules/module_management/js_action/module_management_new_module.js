class module_management_new_module extends ActionHandler{
    constructor(module, action, position_id){
        super(module, action);
        this.position_id = position_id;
    }
    prepareArgs(){
        this.php = true;
    }
    ajax_success(json_str){
        try{
            var obj = JSON.parse(json_str);
            console.log(obj);
            if(obj['status_code'] === 0){
                var ds = obj['module_list'];
                var content='<table>';
                content += "<tr><td>模組</td><td>版本</td><td></td><td></td></tr>";
                for(var key in ds){
                    content += '<tr>';
                    content += '<td>' + key + '</td>';
                    content += '<td>' + ds[key]['module_version'] + '</td>';
                    content += "<td><button id='install'>安裝</button></td>";
                    content += '</tr>';
                }
                content += '</table>';
                this.show_dialog('模組列表', content);
                    
            }
            else{
                $('#'+this.position_id).html(obj['status_message']);
            }
        } catch(e){
            var msg = e + "<br>";
            msg += "JSON String: " + json_str;
            $('#'+this.position_id).html(msg);
        }
    }
    ajax_error(msg){
        $('#'+this.position_id).html(msg.status);
    }
}
            