class home_show_home_page extends ActionHandler{
    constructor(module, action, position_id){
        super(module, action);
        this.position_id = position_id;
    }
    prepareArgs(){
        this.php = true;
    }
    ajax_success(json_str){
        console.log(json_str);
        try{ 
            var obj = JSON.parse(json_str);
            switch(obj['status_code']){
                case 1: //User had logon
                    var str =`
                    <table>
                        <tr>
                            <td>
                                <div style="display: inline" onclick="(new user_profile_show_management_page('user_profile','show_management_page','content')).run()">使用者基資</div>
                            </td>
                            <td>
                                <div style="display: inline" onclick="(new merchant_profile_show_management_page('merchant_profile','show_management_page','content')).run()">商店管理</div>
                            </td>
                            <td>
                                <div style="display: inline" onclick="(new role_management_show_management_page('role_management','show_management_page','content')).run()">角色管理</div>
                            </td>
                            <td>
                                <div style="display: inline" onclick="(new module_management_show_management_page('module_management','show_management_page','content')).run()">模組管理</div>
                            </td>
                        </tr>
                    </table>
                    <div id="content"></div>
                    `;
                    document.getElementById(this.position_id).innerHTML = str;
                    this.loadModuleScript("user_profile", "show_management_page");
                    this.loadModuleScript("merchant_profile", "show_management_page");
                    this.loadModuleScript("role_management", "show_management_page");
                    this.loadModuleScript("module_management", "show_management_page");
                    break;
                case 2: //No user login.
                    var script = this.loadModuleScript("login", "show_login_page");
                    script.onload = function(){
                        (new login_show_login_page("login", "show_login_page", "body")).run();
                    };
                    break;
                default:
                    console.log("Invalid status code"+obj['status_code']);
            }
        }
        catch(e){
            var msg = e + "<br>";
            msg += "JSON String: " + json_str;
            document.getElementById(this.position_id).innerHTML = msg;
        }
    }
}
            