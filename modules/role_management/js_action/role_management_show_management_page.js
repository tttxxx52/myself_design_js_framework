class role_management_show_management_page extends ActionHandler{
    constructor(module, action, position_id){
        super(module, action);
        this.position_id = position_id;
    }
    prepareArgs(){
        this.php = false;
    }
    showResult(xhttp){
        var str=`
        <input type='button' value='新增' onclick="(new role_management_show_insert_page('role_management','show_insert_page','show_area')).run()">
        <input type='button' value='修改' onclick="(new role_management_show_update_list('role_management','show_update_list','show_area')).run()">
        <input type='button' value='刪除' onclick="(new role_management_show_delete_list('role_management','show_delete_list','show_area')).run()">
        <input type='button' value='查詢' onclick="(new role_management_do_select_action('role_management','do_select_action','show_area')).run()">
        <br>
        <div id="show_area"></div>
        `;
        document.getElementById(this.position_id).innerHTML = str;
        
        this.loadModuleScript(this.module, "show_insert_page");
        this.loadModuleScript(this.module, "show_update_list");
        this.loadModuleScript(this.module, "show_delete_list");
        this.loadModuleScript(this.module, "do_select_action");
    }
}
            