class role_management_show_insert_page extends ActionHandler{
    constructor(module, action, position_id){
        super(module, action);
        this.position_id = position_id;
    }
    prepareArgs(){
        this.php = false;
    }
    showResult(xhttp){
        var str=`
        名稱：<input type='text' name='name' id='name' value=''><br/>
        備註：<input type='text' name='comment' id='comment' value=''><br/>
        <input type='button' value='新增' onclick="(new role_management_do_insert_action('role_management','do_insert_action', '%s')).run()">
        `;
        str = str.replace("%s", this.position_id);
        $('#'+this.position_id).html(str);
        this.loadModuleScript(this.module, 'do_insert_action');
    }
}
            