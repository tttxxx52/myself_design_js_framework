class role_management_do_insert_action extends ActionHandler{
    constructor(module, action, position_id){
        super(module, action);
        this.position_id = position_id;
    }
    prepareArgs(){
        this.addArgsById('name');
        this.addArgsById('comment');
    }
    ajax_success(json_str){
        $('#'+this.position_id).html(json_str);
    }
    ajax_error(msg){
        console.log(msg.status);
    }
}