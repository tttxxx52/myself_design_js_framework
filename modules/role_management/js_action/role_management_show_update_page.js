class role_management_show_update_page extends ActionHandler{
    constructor(module, action, position_id){
        super(module, action);
        this.position_id = position_id;
    }
    prepareArgs(){
        this.php_action = 'do_select_action';
        var value = $('input[name=id]:checked').val();
        this.addArgs('where_statement', 'id = ' + value);
    }
    ajax_success(json_str){
        try{ 
            var obj = JSON.parse(json_str);
            if(obj['status_code'] === 0){
                var ds = obj['data_set'];
                var str = "<input type='hidden' name= 'id' id='id' value='" + ds[0]['id'] +"'>";
                str += "姓名：<input type='text' name='name' id='name' value='" + ds[0]['name'] + "'><br/>";
                str += "comment：<input type='text' name='comment' id='comment' value='" + ds[0]['comment'] + "'><br/>";
                str += `<input type='button' value='修改' onclick="(new role_management_do_update_action('role_management','do_update_action', '%s')).run()">`;
                str = str.replace("%s", this.position_id);
                $('#'+this.position_id).html(str);            
                this.loadModuleScript(this.module, "do_update_action");
            }
            else{
                $('#'+this.position_id).html(obj['status_message']);
            }
       } catch(e){
            var msg = e + "<br>";
            msg += "JSON String: " + json_str;
            $('#'+this.position_id).html(msg);
        }
    }
    ajax_error(msg){
        console.log(msg.status);
    }
}            