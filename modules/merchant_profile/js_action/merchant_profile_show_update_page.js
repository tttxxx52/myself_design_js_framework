class merchant_profile_show_update_page extends ActionHandler{
    constructor(module, action, position_id){
        super(module, action);
        this.position_id = position_id;
    }
    prepareArgs(){
        var id = document.getElementsByName('id');
        var value='';
        for(var i=0; i<id.length; i++){
            if(id[i].checked){
                value = id[i].value;
                break;
            }
        }
        this.addArgs('id', value);
        this.php_action = 'do_select_action';
        this.addArgs('where_statement', 'id = ' + value);
    }
    showResult(xhttp){
        if (xhttp.readyState === 4 && xhttp.status === 200) {
            var json_str = xhttp.responseText;
            var obj = JSON.parse(json_str);
            if(obj['error'] === 0){
                var ds = obj['data_set'];
                var str = "<input type='hidden' name='id' id='id' value='" + ds[0]['id'] + "'><br/>";
                str += "商店名稱：<input type='text' name='name' id='name' value='" + ds[0]['name'] + "'><br/>";
                str += "連絡人ID：<input type='text' name='contact_id' id='contact_id' value='" + ds[0]['contact_id'] + "'><br/>";
                str += `<input type='button' value='修改' onclick="(new merchant_profile_do_update_action('merchant_profile','do_update_action', '%s')).run()">`;
                str = str.replace("%s", this.position_id);
                document.getElementById(this.position_id).innerHTML = str;

                this.loadModuleScript(this.module, "do_update_action");
            }
            else{
                document.getElementById(this.position_id).innerHTML = obj['error_message'];
            }
       }
        else{
            document.getElementById(this.position_id).innerHTML = xhttp.statusText;
        }
    }
}            