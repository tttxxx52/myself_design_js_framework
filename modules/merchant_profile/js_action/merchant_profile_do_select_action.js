class merchant_profile_do_select_action extends ActionHandler{
    constructor(module, action, position_id){
        super(module, action);
        this.position_id = position_id;
    }
    prepareArgs(){
        this.php = true;
        this.addArgs('where_statement', '');
    }
    showResult(xhttp){
        var json_str;
        if (xhttp.readyState === 4 && xhttp.status === 200) {
            json_str = xhttp.responseText;
            var obj = JSON.parse(json_str);
            if(obj['error'] === 0){
                var content='';
                var ds = obj['data_set'];
                for(var index in ds){
                    for(var key in ds[index]){
                        content += ds[index][key] + ' ';
                    }
                    content += '<br>';
                }
                document.getElementById(this.position_id).innerHTML = content;
            }
            else{
                document.getElementById(this.position_id).innerHTML = obj['error_message'];
            }
        }
        else{
            document.getElementById(this.position_id).innerHTML = xhttp.statusText;
        }
    }
}