class merchant_profile_show_delete_list extends ActionHandler{
    constructor(module, js_action, position_id){
        super(module, js_action);
        this.position_id = position_id;
    }
    prepareArgs(){
        this.php = true;
        this.php_action = 'do_select_action';
        this.addArgs('where_statement', '');
    }
    showResult(xhttp){
        if (xhttp.readyState === 4 && xhttp.status === 200) {
            var json_str = xhttp.responseText;
            var obj = JSON.parse(json_str);
            if(obj['error'] === 0){
                var content='';
                var ds = obj['data_set'];
                for(var index in ds){
                    content += "<input type=radio name='id' id='id' value=" + ds[index]['id'] +">" + " ";
                    for(var key in ds[index]){
                        if(key !== 'id'){
                            content += ds[index][key] + ' ';
                        }
                    }
                    content += '<br>';
                }
                var str=`
                    <input type='button' value='刪除' onclick="(new merchant_profile_do_delete_action('merchant_profile','do_delete_action','%s')).run()">
                `;
                str = str.replace("%s", this.position_id);
                content += str;
                document.getElementById(this.position_id).innerHTML = content;

                this.loadModuleScript(this.module, 'do_delete_action');
            }
            else{
                document.getElementById(this.position_id).innerHTML = obj['error_message'];
            }
        }
        else{
            document.getElementById(this.position_id).innerHTML = xhttp.statusText;
        }
    }
}            