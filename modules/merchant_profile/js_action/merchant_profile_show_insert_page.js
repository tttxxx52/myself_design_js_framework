class merchant_profile_show_insert_page extends ActionHandler{
    constructor(module, action, position_id){
        super(module, action);
        this.position_id = position_id;
    }
    prepareArgs(){
        this.php = false;
    }
    showResult(xhttp){
        var str=`
        商店名稱：<input type='text' name='name' id='name' value=''><br/>
        連絡人 email:<input type='text' name='email' id='email' value='' onkeyup="(new member_profile_get_email_hint('member_profile','get_email_hint', '%s1')).run()"><br/>
        Suggestion: <div id='email_hint'></div>
        <input type='button' value='新增' onclick="(new merchant_profile_do_insert_action('merchant_profile','do_insert_action', '%s2')).run()">
        `;
        str = str.replace("%s1", 'email_hint');
        str = str.replace("%s2", this.position_id);
        document.getElementById(this.position_id).innerHTML = str;
        
        this.loadModuleScript(this.module, 'do_insert_action');
        this.loadModuleScript('member_profile', 'get_email_hint');
    }
}
            